#!/bin/bash

TEMP="../temp"

if [[ ! -d $TEMP ]]; then
    mkdir $TEMP
fi

cd $TEMP

wget -r --no-parent https://play.pokemonshowdown.com/audio/cries/ || ( echo "Cannot connect to play.pokemonshowdown.com!"; exit )

rm -rf ../cries

cp -r ./play.pokemonshowdown.com/audio/cries ../cries

rm -rf ../cries/src
rm ../cries/*.ogg

rm -rf $TEMP

exit
